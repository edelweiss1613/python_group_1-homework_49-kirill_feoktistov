from django.urls import path
from webapp.views import *

app_name = 'webapp'

urlpatterns = [
    path('', MainPageView.as_view(), name='index'),
    path('food', FoodListView.as_view(), name='food_list'),
    path('food/<int:pk>', FoodDetailView.as_view(), name='food_detail'),
    path('food/<int:pk>/update', FoodUpdateView.as_view(), name='food_update'),
    path('food/<int:pk>/delete', FoodDeleteView.as_view(), name='food_delete'),
    path('food/create', FoodCreateView.as_view(), name='food_create'),
    path('order/<int:pk>', OrderDetailView.as_view(), name='order_detail'),
    path('order/create', OrderCreateView.as_view(), name='order_create'),
    path('order/<int:pk>/update', OrderUpdateAjaxView.as_view(), name='order_update'),
    path('order/<int:pk>/food/create', OrderFoodCreateAjaxView.as_view(), name='order_food_create'),
    # path('order/<int:pk>/food/create_ajax', OrderFoodCreateAjaxView.as_view, name='order_food_create_ajax'),
    path('order/<int:pk>/food/<int:food_pk>/delete', OrderFoodDeleteAjaxView.as_view(), name='order_food_delete'),
    path('order_food/<int:pk>/update', OrderFoodUpdateAjaxView.as_view(), name='order_food_update'),
    path('order/<int:pk>/status', OrderDeliverView.as_view(), name='order_deliver'),
    path('order/<int:pk>/cancel', OrderRejectView.as_view(), name='order_cancel'),
]


